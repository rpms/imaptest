%global git_commit 074d3e36c78131f0a7d606233f8466209623df55
%global git_date 20200324

%global git_short_commit %(echo %{git_commit} | cut -c -8)
%global git_suffix %{git_date}git%{git_short_commit}


Summary:	A generic IMAP server compliancy tester
Name:		imaptest
# Upstream is not really planning on adding version numbers
Version:	20200324
Release:        2.%{git_suffix}%{?dist}
License:	MIT
Group:		Applications/Internet
URL:		https://www.imapwiki.org/ImapTest

##  core=2.3 branch
Source0:        https://github.com/dovecot/%{name}/archive/%{git_commit}/%{name}-%{version}.tar.gz
BuildRequires:	dovecot-devel >= 2.2.15
BuildRequires:	autoconf, automake, libtool
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
ImapTest is a generic IMAP server compliancy tester that works with all IMAP
servers. It supports stress testing with state tracking, scripted testing and
benchmarking. When stress testing with state tracking ImapTest sends random
commands to the server and verifies that server's output looks correct. Using
the scripted testing ImapTest runs a list of predefined scripted tests and
verifies that server returns expected output.

Examples and details are provided online at: http://www.imapwiki.org/ImapTest

%prep
%autosetup -n %{name}-%{git_commit}
autoreconf -i

# Workaround for https://bugzilla.redhat.com/show_bug.cgi?id=1103927#c4 (and later)
sed -e 's@\(^LIBDOVECOT .*\)@\1 -Wl,-rpath -Wl,%{_libdir}/dovecot@' -i src/Makefile.in

%build
%configure --with-dovecot=%{_libdir}/dovecot
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

# Copy test files for later shipping
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}/
cp -pr src/tests/ $RPM_BUILD_ROOT%{_datadir}/%{name}/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{!?_licensedir:%global license %%doc}
%license COPYING COPYING.MIT
%doc AUTHORS NEWS profile.conf pop3-profile.conf
%{_bindir}/%{name}
%{_datadir}/%{name}/

%changelog
* Tue Apr 21 2020  <pavel@pzhukov-laptop.zhukoff.net> - 20200324-2
- Resolves: #1680950 - Test gating

* Tue Mar 24 2020  <pavel@pzhukov-laptop.zhukoff.net> - 20200324-1
- Resolves: #1776856 - Rebase for new dovecot API

* Tue Mar 24 2020  <pavel@pzhukov-laptop.zhukoff.net> - 20170719-3
- Resolves: #1776856 - Rebuild with new dovecot

* Wed Feb 07 2018 Fedora Release Engineering <releng@fedoraproject.org> - 20170719-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Sun Aug 06 2017 Robert Scheck <robert@fedoraproject.org> 20170719-1
- Upgrade to 20170719

* Wed Aug 02 2017 Fedora Release Engineering <releng@fedoraproject.org> - 20151228-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 20151228-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 20151228-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 20151228-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Sun Jan 03 2016 Robert Scheck <robert@fedoraproject.org> 20151228-1
- Upgrade to 20151228

* Sun Dec 27 2015 Robert Scheck <robert@fedoraproject.org> 20151210-1
- Upgrade to 20151210

* Mon Oct 12 2015 Robert Scheck <robert@fedoraproject.org> 20151007-1
- Upgrade to 20151007

* Fri Aug 14 2015 Robert Scheck <robert@fedoraproject.org> 20150812-1
- Upgrade to 20150812

* Wed Jun 24 2015 Robert Scheck <robert@fedoraproject.org> 20150620-1
- Upgrade to 20150620

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20141030-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Fri Nov 07 2014 Robert Scheck <robert@fedoraproject.org> 20141030-1
- Upgrade to 20141030

* Sat Aug 16 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20140711-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Wed Jul 23 2014 Robert Scheck <robert@fedoraproject.org> 20140711-1
- Upgrade to 20140711

* Fri Jun 20 2014 Robert Scheck <robert@fedoraproject.org> 20140528-2
- Added workaround for missing rpath linking (#1103927 #c4)

* Tue Jun 03 2014 Robert Scheck <robert@fedoraproject.org> 20140528-1
- Upgrade to 20140528
- Initial spec file for Fedora and Red Hat Enterprise Linux
